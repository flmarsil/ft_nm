#include "../includes/ft_nm.h"

void unrecognized_option(char* flag) {
    if (!flag)
        return ;

    char* msg = "unrecognized option '";
    write(STDERR_FILENO, "ft_nm: ", 7);
    write(STDERR_FILENO, msg, ft_strlen(msg));
    write(STDERR_FILENO, flag, ft_strlen(flag));
    write(STDERR_FILENO, "'\n", 2);
}

void invalid_option(char flag) {
    if (!flag)
        return ;

    char* msg = "invalid option -- '";
    write(STDERR_FILENO, "ft_nm: ", 7);
    write(STDERR_FILENO, msg, ft_strlen(msg));
    write(STDERR_FILENO, &flag, 1);
    write(STDERR_FILENO, "'\n", 2);
}


int quick_check_double_dash_help_flag(int ac, char** av) {
    for (int index = 1; index < ac; index++) {
        if (ft_memcmp(av[index], "--", 2) == SUCCESS) {
            if (ft_charcmp(av[index][2], 'h') == FAILURE)
                unrecognized_option(av[index]);
            /*
                TODO: print_help_page();
            */
            printf("PRINT HELP PAGE\n");
            return (SUCCESS);
        }
    }
    return (FAILURE);
}

int check_options_flags(char flag) {
    char flags[NB_FLAGS] = { 'a', 'g', 'u', 'r', 'p' };

    for (int i = 0; i < NB_FLAGS; i++) {
        if (ft_charcmp(flags[i], flag) == SUCCESS) {
            /*
                TODO: Si structure globale, ajouter le flag
            */
            return (SUCCESS);
        }
    }
    return (FAILURE);
}

int quick_check_simple_dash_help_flag(int ac, char** av) {
    for (int index = 1; index < ac; index++) {
        if (ft_memcmp(av[index], "-", 1) == SUCCESS) {
            for (int x = 1; av[index][x]; x++) {
                if (ft_charcmp(av[index][x], 'h') == SUCCESS) {
                /*
                    TODO: print_help_page();
                */ 
                    printf("PRINT HELP PAGE\n");
                    return (SUCCESS);
                }
            }
            for (int x = 1; av[index][x]; x++) {
                if (check_options_flags(av[index][x]) == FAILURE) {
                    invalid_option(av[index][x]);
                /*
                    TODO: print_help_page();
                */ 
                    printf("PRINT HELP PAGE\n");
                    return (SUCCESS);
                }
            }
        }
    }
    return (FAILURE);
}
