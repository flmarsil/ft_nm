#include "../includes/ft_nm.h"

int ft_memcmp(const void *s1, const void *s2, size_t n) {
	size_t i;

	i = -1;
	while (++i < n) {
		if (((unsigned char *)s1)[i] != (((unsigned char *)s2)[i]))
			return (((unsigned char *)s1)[i] - (((unsigned char *)s2)[i]));
	}
	return (0);
}

void* ft_memset(void *b, int c, size_t len) {
	size_t i;

	i = -1;
	while (++i < len)
		((unsigned char *)b)[i] = (unsigned char)c;
	return (b);
}


int ft_strcmp(const char *s1, const char *s2) {
	size_t i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] && s2[i])
		i++;
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}

int ft_strlen(char* s) {
    if (!s)
        return (0);
    int i = 0;
    for (; s[i]; i++);
    return (i);
}


int ft_charcmp(char a, char b) {
    return ((a == b) ? 0 : 1);
}

void leave_properply(t_file* file) {
	if (file->fd && close(file->fd) == -1)
		write(STDERR_FILENO, "Close has been failed\n", 22);
	if (file->addr && munmap(file->addr, file->size) == -1)
		write(STDERR_FILENO, "Munmap has been failed\n", 23);
}

void error_message(char* filename, char* msg, t_file* file) {
	if (file)
		leave_properply(file);
	if (!msg || !filename)
		return ;
	write(STDERR_FILENO, "ft_nm: '", 8);
	write(STDERR_FILENO, filename, ft_strlen(filename));
	write(STDERR_FILENO, "': ", 3);
	write(STDERR_FILENO, msg, ft_strlen(msg));
	write(STDERR_FILENO, "\n", 1);
}