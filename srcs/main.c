#include "../includes/ft_nm.h"

void nm_push_back(t_nm* nm, t_file* file) {
    t_nm* tmp = nm;

    if (!nm->files)
        nm->files = file;
    else {
        for (; tmp->files->next ; tmp->files = tmp->files->next);
        tmp->files->next = file;
    }
}

void check_arguments(char* av, t_nm* nm) {
    struct stat buffer;
    t_file file;
    (void)nm;

    if (!av)
        return ;
    ft_memset(&file, 0, sizeof(t_file*));
    if ((file.fd = open(av, O_RDONLY, S_IRUSR)) == -1)
        return (error_message(av, "No such file", 0));
    else if (fstat(file.fd, &buffer) == -1)
        return (error_message(av, "Error fstat()", &file));
    else if (S_ISDIR(buffer.st_mode))
        return (error_message(av, "is a directory", &file));
    /* Verifie si le fichier est vide */
    else if ((file.size = buffer.st_size) == 0)
        return (error_message(NULL, NULL, &file));
    /* Chargement du fichier dans la memoire */
    else if ((file.addr = mmap(NULL, file.size, PROT_READ, MAP_PRIVATE, file.fd, 0)) == MAP_FAILED)
        return (error_message(NULL, NULL, &file));
    /* Lancement du parsing */
    char* err = parse_elf_header(&file);
    if (err != NULL)
        return (error_message(av, err, &file));
    nm_push_back(nm, &file);
    if (file.fd)
        leave_properply(&file);
    return ;
}

int nm_init(t_nm* nm) {
    ft_memset(nm, 0, sizeof(t_nm*));

    // verif flags
    // malloc + memset les structures nm + file ? + display ?
    return(SUCCESS);
}

int main(int ac, char** av) {
    t_nm nm;

    if (nm_init(&nm) == FAILURE)
        return (EXIT_FAILURE);

    // mettre regexp tiret + lettres min maj uniquement sur les arguments pour eviter des buffers overflow
    if (ac == 1 || (ac == 2 && ft_strcmp(av[1], "--") == SUCCESS))
        check_arguments("a.out", &nm);
    else if (quick_check_double_dash_help_flag(ac, av) == SUCCESS)
        return (EXIT_SUCCESS);
    else if (quick_check_simple_dash_help_flag(ac, av) == SUCCESS)
        return (EXIT_SUCCESS);
    else {
        for (int i = 1; i < ac; i++)
            check_arguments(av[i], &nm);
    }
    return (EXIT_SUCCESS);
}
