#include "../includes/ft_nm.h"

char* elf_x86_32_handler(t_file* file) {
    printf("\033[35;01mx32 handler has been launched\n\033[00m");
    t_elf_32 elf;
    
    elf.ehdr = (Elf32_Ehdr*)file->addr;

    (void)elf;
    return (SUCCESS);
}