CC					= gcc
CFLAGS				= -Wall -Wextra -Werror
RM 					= rm -rf

DIR_FT_NM 			= .
SRCS_FT_NM 			= $(DIR_FT_NM)/srcs/
INCS_FT_NM 			= $(DIR_FT_NM)/includes/
OBJS_FT_NM			= $(DIR_FT_NM)/objs/

SOURCES				= 	main.c \
						utils.c \
						parsing_flags.c \
						parsing_elf_header.c \
						elf_x64_handler.c \
						elf_x86_32_handler.c

OBJS 				= $(SOURCES:%.c=$(OBJS_FT_NM)%.o)
TARGET 				= ft_nm

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET)

# compiled sources :
$(OBJS): | $(OBJS_FT_NM)

$(OBJS_FT_NM)%.o: $(SRCS_FT_NM)%.c
	$(CC) $(CFLAGS) -I $(INCS_FT_NM) -c $< -o $@

$(OBJS_FT_NM):
	@mkdir -p $(OBJS_FT_NM)

clean: 
	@$(RM) $(OBJS_FT_NM)

fclean: clean
	@$(RM) $(TARGET)

re: fclean all

.PHONY: all clean fclean re

