#ifndef ELF_X86_32_H
#define ELF_X86_32_H

#include "elf.h"

typedef struct      s_elf_32 {
    Elf32_Ehdr*     ehdr;
    Elf32_Shdr*     shdr;
    Elf32_Phdr*     phdr;
}                   t_elf_32; 

#endif
